import UIKit

class CharacterDetailViewController: UIViewController {

    // MARK: - Private properties
    private var viewModel: CharacterDetailViewModelling

    // MARK: - Initializers
    init(viewModel: CharacterDetailViewModelling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views
    private lazy var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 2.0/3.0).isActive = true
        imageView.clipsToBounds = true
        return imageView
    }()

    private lazy var occupationLabel: DescriptiveLabel = {
        let occupationLabel = DescriptiveLabel(frame: .zero)
        occupationLabel.descriptionLabel.text = "Occupation"
        occupationLabel.contentLabel.text = viewModel.occupation
        occupationLabel.translatesAutoresizingMaskIntoConstraints = false
        return occupationLabel
    }()

    private lazy var statusLabel: DescriptiveLabel = {
        let statusLabel = DescriptiveLabel(frame: .zero)
        statusLabel.descriptionLabel.text = "Status"
        statusLabel.contentLabel.text = viewModel.status
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        return statusLabel
    }()

    private lazy var nickNameLabel: DescriptiveLabel = {
        let nickNameLabel = DescriptiveLabel(frame: .zero)
        nickNameLabel.descriptionLabel.text = "Nickname"
        nickNameLabel.contentLabel.text = viewModel.nickname
        nickNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nickNameLabel
    }()

    private lazy var seasonsAppearedLabel: DescriptiveLabel = {
        let seasonsAppearedLabel = DescriptiveLabel(frame: .zero)
        seasonsAppearedLabel.descriptionLabel.text = "Seasons Appeared"
        seasonsAppearedLabel.contentLabel.text = viewModel.seasonsAppeared
        seasonsAppearedLabel.translatesAutoresizingMaskIntoConstraints = false
        return seasonsAppearedLabel
    }()


    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureBlurBackground()
        configureDetailView()
        viewModel.handle(.viewDidLoad)
    }

    // MARK: - View Config
    private func configureNavigationBar() {
        self.navigationItem.title = viewModel.name
    }

    private func configureBlurBackground() {
        view.addSubview(backgroundImageView)
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backgroundImageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            backgroundImageView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])

        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(blurView)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: view.topAnchor),
            blurView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            blurView.leftAnchor.constraint(equalTo: view.leftAnchor),
            blurView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }

    private func configureDetailView() {
        let scrollview = UIScrollView(frame: .zero)
        scrollview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollview)
        NSLayoutConstraint.activate([
            scrollview.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollview.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollview.leftAnchor.constraint(equalTo: view.leftAnchor),
            scrollview.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])

        let stackView = configureDetailStackView()
        scrollview.addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20.0),
            stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20.0)
        ])
    }

    private func configureDetailStackView() -> UIStackView {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        stackView.addArrangedSubview(configureImageStackView())
        stackView.addArrangedSubview(configureLabelsStackView())
        return stackView
    }

    private func configureImageStackView() -> UIStackView {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.addArrangedSubview(Spacer(.vertical, space: 60.0))
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(Spacer(.vertical, space: 60.0))
        return stackView
    }

    private func configureLabelsStackView() -> UIStackView {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        stackView.addArrangedSubview(occupationLabel)
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        stackView.addArrangedSubview(statusLabel)
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        stackView.addArrangedSubview(nickNameLabel)
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        stackView.addArrangedSubview(seasonsAppearedLabel)
        stackView.addArrangedSubview(Spacer(.horizontal, space: 20.0))
        return stackView
    }
}

extension CharacterDetailViewController: CharaterDetailViewModelDelegate {
    func imageFetched(_ image: UIImage?) {
        backgroundImageView.image = image
        imageView.image = image
    }
}

import UIKit

class Character: Codable, Identifiable {
    let id: Int
    let name: String
    let occupation: [String]
    let imageUrlPath: String
    let status: String
    private let optionalAppearance: [Int]?
    var appearance: [Int] {
        optionalAppearance ?? []
    }
    let nickname: String

    enum CodingKeys: String, CodingKey {
        case id = "char_id"
        case name
        case occupation
        case imageUrlPath = "img"
        case status
        case optionalAppearance = "appearance"
        case nickname
    }

    init(id: Int,
         name: String,
         occupation: [String],
         imageUrlPath: String,
         status: String,
         optionalAppearance: [Int]?,
         nickname: String) {
        self.id = id
        self.name = name
        self.occupation = occupation
        self.imageUrlPath = imageUrlPath
        self.status = status
        self.optionalAppearance = optionalAppearance
        self.nickname = nickname
    }
}

extension Character: Equatable, Hashable {
    static func == (lhs: Character, rhs: Character) -> Bool {
        return lhs.id == rhs.id &&
        lhs.name == rhs.name &&
        lhs.occupation == rhs.occupation &&
        lhs.imageUrlPath == rhs.imageUrlPath &&
        lhs.status == rhs.status &&
        lhs.optionalAppearance == rhs.optionalAppearance &&
        lhs.nickname == rhs.nickname
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

import Foundation

typealias DataTaskCompletionHandler = (Data?, URLResponse?, Error?) -> Void
typealias ResultHandler<T> = (Result<T, BBError>) -> ()
typealias DataParser<T> = (Data) -> Result<T, BBError>

protocol BBAppURLSession {
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionDataTask
}

extension URLSession: BBAppURLSession { }

protocol NetworkRequestHandlerType {
    func execute<T>(urlRequest: URLRequest?,
                    parser: @escaping DataParser<T>,
                    completion: @escaping ResultHandler<T>)

}
class NetworkRequestHandler: NetworkRequestHandlerType {

    private let defaultSession: BBAppURLSession
    private var task: URLSessionDataTask?

    init(defaultSession: BBAppURLSession = URLSession(configuration: .default)) {
        self.defaultSession = defaultSession
    }

    func execute<T>(urlRequest: URLRequest?,
                    parser: @escaping DataParser<T>,
                    completion: @escaping ResultHandler<T>) {
        guard let urlRequest = urlRequest else {
            DispatchQueue.main.async { completion(.failure(BBError.unknownError)) }
            return
        }
        task = defaultSession.dataTask(with: urlRequest) { [weak self] data, response, error in
            let result: Result<T, BBError>
            defer { self?.task = nil }
            if error != nil {
                result = .failure(.unknownError)
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                result = parser(data)
            } else {
                result = .failure(.invalidResponse)
            }
            DispatchQueue.main.async { completion(result) }
        }
        task?.resume()
    }
}

import Foundation
import UIKit

protocol BreakingBadAPIType {
    func getCharacters(completion: @escaping ResultHandler<[Character]>)
}

struct BreakingBadAPI: BreakingBadAPIType {

    private let networkRequestHandler: NetworkRequestHandlerType

    init(networkRequestHandler: NetworkRequestHandlerType = NetworkRequestHandler()) {
        self.networkRequestHandler = networkRequestHandler
    }

    func getCharacters(completion: @escaping ResultHandler<[Character]>) {
        execute(urlRequest: BreakingBadAPIRouter.getCharacters.asURLRequest(),
                completion: completion)
    }

    private func execute<T: Codable>(urlRequest: URLRequest?, completion: @escaping ResultHandler<T>) {
        DispatchQueue.global(qos: .userInitiated).async {
            networkRequestHandler.execute(urlRequest: urlRequest,
                                          parser: JSONParser<T>().parse,
                                          completion: completion)
        }
    }
}


import Foundation

enum BreakingBadAPIRouter {

    case getCharacters

    private var path: String {
        switch self {
        case .getCharacters:
            return "characters"
        }
    }

    func asURLRequest() -> URLRequest? {
        let baseURLStr = "https://breakingbadapi.com/api/"
        let urlcomponents = URLComponents(string: baseURLStr + self.path)
        guard let url = urlcomponents?.url else {
            return nil
        }
        return URLRequest(url: url)
    }
}

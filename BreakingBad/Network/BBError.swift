enum BBError: Error {
    case unknownError
    case parseError
    case invalidResponse
    case inavalidImageData

    var errorDescription: String? {
        switch self {
        case .unknownError:
            return "🤯 We have no clue what is happening!!"
        case .parseError:
            return "🙃 Sorry!! The server has been sending stuff I can't understand"
        case .invalidResponse:
            return "😩 Sorry!! The server has gone nuts."
        case .inavalidImageData:
            return "Could not parse the data as an Image"
        }
    }
}

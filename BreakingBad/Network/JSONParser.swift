import Foundation

class JSONParser<T: Codable> {

    func parse(_ data: Data) -> Result<T, BBError> {

        let decoder = JSONDecoder()

        do {
            let objectOfTypeT = try decoder.decode(T.self, from: data)
            return .success(objectOfTypeT)
        }
        catch {
            return .failure(BBError.parseError)
        }
    }
}

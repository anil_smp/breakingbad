import UIKit

class SeasonFilterCell: UITableViewCell {

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        configureImageView()
        configureLabel()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Views
    private lazy var imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.tintColor = .orange
        return imgView
    }()

    private lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: - View Setup
    private func configureImageView() {
        addSubview(imgView)
        NSLayoutConstraint.activate([
            imgView.rightAnchor.constraint(equalTo: rightAnchor, constant: -20.0),
            imgView.heightAnchor.constraint(equalToConstant: 24.0),
            imgView.widthAnchor.constraint(equalToConstant: 24.0),
            imgView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }

    private func configureLabel() {
        addSubview(label)
        NSLayoutConstraint.activate([
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 20.0),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }

    // MARK: - Public interface
    func configure(_ cellViewModel: SeasonFilterCellViewModelling) {
        label.text = cellViewModel.season
        imgView.image = cellViewModel.isSelected.image?.withRenderingMode(.alwaysTemplate)
    }

}

private extension Bool {
    var image: UIImage? {
        if self {
            return UIImage(systemName: "checkmark.circle.fill")
        } else {
            return UIImage(systemName: "circle")
        }
    }
}

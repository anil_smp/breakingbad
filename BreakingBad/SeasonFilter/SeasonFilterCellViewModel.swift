import Foundation

protocol SeasonFilterCellViewModelling {
    var season: String { get }
    var isSelected: Bool { get }
}

struct SeasonFilterCellViewModel: SeasonFilterCellViewModelling {
    let seasonNum: String
    let isSelected: Bool

    var season: String {
        "Season " + seasonNum
    }
}

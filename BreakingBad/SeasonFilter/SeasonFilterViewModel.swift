import Foundation
import UIKit

enum SeasonFilterViewEvent {
    case didTapItemAt(index: Int)
    case clearTapped
    case closeTapped
}

protocol SeasonFilterViewModelling {
    var numberOfItems: Int { get }
    var delegate: SeasonFilterViewModelDelegate? { get set }
    func cellViewModel(at index: Int) -> SeasonFilterCellViewModelling
    func handle(_ event: SeasonFilterViewEvent)
}

protocol FilterDelegate {
    var selectedSeasons: [Int] { get set}
}

protocol SeasonFilterViewModelDelegate {
    func refreshAllData()
    func refreshData(at index: Int)
}

class SeasonFilterViewModel: SeasonFilterViewModelling {

    private let allSeasons = [1, 2, 3, 4, 5]
    private var filterDelegate: FilterDelegate
    private let coordinator: ApplicationCoordinatorType

    var delegate: SeasonFilterViewModelDelegate?

    init(filterDelegate: FilterDelegate,
         coordinator: ApplicationCoordinatorType) {
        self.filterDelegate = filterDelegate
        self.coordinator = coordinator
    }

    var numberOfItems: Int {
        allSeasons.count
    }

    func cellViewModel(at index: Int) -> SeasonFilterCellViewModelling {
        SeasonFilterCellViewModel(
            seasonNum: "\(allSeasons[index])",
            isSelected: filterDelegate.selectedSeasons.contains(allSeasons[index])
        )
    }

    func handle(_ event: SeasonFilterViewEvent) {
        switch event {
        case .didTapItemAt(let index):
            let season = allSeasons[index]
            if filterDelegate.selectedSeasons.contains(season) {
                filterDelegate.selectedSeasons.removeAll(where: { $0 == season })
            } else {
                filterDelegate.selectedSeasons.append(season)
            }
            delegate?.refreshData(at: index)

        case .clearTapped:
            filterDelegate.selectedSeasons = []
            delegate?.refreshAllData()

        case .closeTapped:
            coordinator.dismissSeasonFilter()
        }
    }
}

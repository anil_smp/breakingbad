import UIKit

class SeasonFilterViewController: UIViewController {

    // MARK: - Private properties
    private var viewModel: SeasonFilterViewModelling

    // MARK: - Initializers
    init(viewModel: SeasonFilterViewModelling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views

    private lazy var blurView: UIVisualEffectView = {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.translatesAutoresizingMaskIntoConstraints = false
        return blurView
    }()

    private lazy var tableView: UITableView = {
        let tableView =  UITableView(frame: .zero)
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SeasonFilterCell.self,
                           forCellReuseIdentifier: SeasonFilterCell.reuseIdentifier)
        return tableView
    }()

    private lazy var closeBarButtonItem: UIBarButtonItem = {
        let image = UIImage(systemName: "xmark")?.withRenderingMode(.alwaysTemplate)
        return UIBarButtonItem(image: image,
                               style: .plain,
                               target: self, action: #selector(didTapClose(_:)))
    }()

    private lazy var clearFilterButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .orange
        button.setTitleColor(.orange, for: .normal)
        button.setTitleColor(UIColor.orange.withAlphaComponent(0.5), for: .highlighted)
        button.setTitle("Clear Filters", for: .normal)
        button.addTarget(self,
                         action: #selector(didTapClearFilter(_:)),
                         for: .touchUpInside)
        return button
    }()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        configureNavigationBar()
        configureBlurView()
        configureTableView()
        configureClearFilterButton()
    }

    // MARK: - View Config
    private func configureNavigationBar() {
        self.navigationItem.title = "Season Appeared filter"
        self.navigationItem.rightBarButtonItem = closeBarButtonItem
    }

    private func configureBlurView() {
        view.addSubview(blurView)
        NSLayoutConstraint.activate([
            blurView.topAnchor.constraint(equalTo: view.topAnchor),
            blurView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            blurView.leftAnchor.constraint(equalTo: view.leftAnchor),
            blurView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }

    private func configureTableView() {
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.heightAnchor.constraint(equalToConstant: CGFloat(viewModel.numberOfItems) * 44.0)
        ])
    }

    private func configureClearFilterButton() {
        view.addSubview(clearFilterButton)
        NSLayoutConstraint.activate([
            clearFilterButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 20.0),
            clearFilterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])

    }

    // MARK: - Actions
    @objc private func didTapClose(_ sender: UIBarButtonItem) {
        viewModel.handle(.closeTapped)
    }

    @objc private func didTapClearFilter(_ sender: UIButton) {
        viewModel.handle(.clearTapped)
    }
}

// MARK: - UITableViewDelegate
extension SeasonFilterViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.handle(.didTapItemAt(index: indexPath.row))
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
}

// MARK: - UITableViewDataSource
extension SeasonFilterViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfItems
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SeasonFilterCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(viewModel.cellViewModel(at: indexPath.row))
        return cell
    }
}

// MARK: - SeasonFilterViewModelDelegate
extension SeasonFilterViewController: SeasonFilterViewModelDelegate {
    func refreshAllData() {
        tableView.reloadData()
    }

    func refreshData(at index: Int) {
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}

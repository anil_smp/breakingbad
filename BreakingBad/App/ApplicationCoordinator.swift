import UIKit

protocol ApplicationCoordinatorType {
    func launchSeasonFilter(filterDelegate: FilterDelegate)
    func launchCharacterDetail(for character: Character,
                               imageFetcher: ImageFetcher)
    func dismissSeasonFilter()
}

class ApplicationCoordinator: Coordinator {

    private let rootViewController: UINavigationController

    override init() {
        rootViewController = UINavigationController()
        super.init()
    }

    func start(on window: UIWindow) {
        let characterListVM = CharacterListViewModel(coordinator: self,
                                                    api: BreakingBadAPI())
        rootViewController.viewControllers = [
            CharacterListViewController(viewModel: characterListVM)
        ]
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}

extension ApplicationCoordinator: ApplicationCoordinatorType {
    func launchSeasonFilter(filterDelegate: FilterDelegate) {
        let seasonFilterVM = SeasonFilterViewModel(filterDelegate: filterDelegate,
                                                   coordinator: self)
        let seasonFilterVC = SeasonFilterViewController(viewModel: seasonFilterVM)
        seasonFilterVC.modalPresentationStyle = .formSheet
        rootViewController.present(UINavigationController(rootViewController: seasonFilterVC),
                                   animated: true,
                                   completion: nil)
    }

    func launchCharacterDetail(for character: Character,
                               imageFetcher: ImageFetcher) {
        let characterDetailVM = CharacterDetailViewModel(character: character,
                                                         imageFetcher: imageFetcher)
        rootViewController.pushViewController(CharacterDetailViewController(viewModel: characterDetailVM),
                                              animated: true)
    }

    func dismissSeasonFilter() {
        rootViewController.dismiss(animated: true, completion: nil)
    }
}

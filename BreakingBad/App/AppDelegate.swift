import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupAppStyling()
        return true
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}

private func setupAppStyling() {
    UINavigationBar.appearance().barStyle = .black
    UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
    UIBarButtonItem.appearance().tintColor = .orange
}


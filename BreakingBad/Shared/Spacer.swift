import UIKit

class Spacer: UIView {

    enum SpacerType {
        case vertical
        case horizontal
    }

    private let type: SpacerType
    private let space: CGFloat


    init(_ type: SpacerType, space: CGFloat) {
        self.type = type
        self.space = space
        super.init(frame: .zero)
        self.setupView()

    }

    override init(frame: CGRect) {
        fatalError("init(frame:) has not been implemented")
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        switch type {
        case .vertical:
            widthAnchor.constraint(equalToConstant: space).isActive = true
        case .horizontal:
            heightAnchor.constraint(equalToConstant: space).isActive = true
        }
    }
}

import Foundation
import UIKit

protocol ImageFetcher {
    func fetchImageAsync(for character: Character, completion: ((UIImage?) -> Void)?)
    func fetchedImage(for character: Character) -> ImageState
    func cancelFetch(for character: Character)
}

class AsyncFetcher: ImageFetcher {

    /// A serial `OperationQueue` to lock access to the `fetchQueue` and `completionHandlers` properties.
    private let serialAccessQueue = OperationQueue()

    /// An `OperationQueue` that contains `AsyncFetcherOperation`s for requested data.
    private let fetchQueue = OperationQueue()

    /// A dictionary of arrays of closures to call when an object has been fetched for an id.
    private var completionHandlers = [Character: (UIImage?) -> Void]()

    /// An `NSCache` used to store fetched objects.
    private var cache = NSCache<Character, UIImage>()

    // MARK: Initialization

    init() {
        serialAccessQueue.maxConcurrentOperationCount = 1
    }

    // MARK: Object fetching
    func fetchImageAsync(for character: Character, completion: ((UIImage?) -> Void)? = nil) {
        // Use the serial queue while we access the fetch queue and completion handlers.
        serialAccessQueue.addOperation {
            // If a completion block has been provided, store it.
            if let completion = completion {
                self.completionHandlers[character] = completion
            }
            
            self.fetchImage(for: character)
        }
    }

    func fetchedImage(for character: Character) -> ImageState {
        guard let image =  cache.object(forKey: character) else {
            return .loading
        }
        return .loaded(image)
    }

    func cancelFetch(for character: Character) {
        serialAccessQueue.addOperation {
            self.fetchQueue.isSuspended = true
            defer {
                self.fetchQueue.isSuspended = false
            }

            self.operation(for: character)?.cancel()
            self.completionHandlers[character] = nil
        }
    }

    // MARK: Convenience

    private func fetchImage(for character: Character) {
        // If a request has already been made for the object, do nothing more.
        guard operation(for: character) == nil else { return }
        
        if case .loaded(let image) = fetchedImage(for: character) {
            // The object has already been cached; call the completion handler with that object.
            invokeCompletionHandler(for: character, with: image)
        } else {
            // Enqueue a request for the object.
            let operation = AsyncFetcherOperation(character: character)
            
            // Set the operation's completion block to cache the fetched object and call the associated completion blocks.
            operation.completionBlock = { [weak operation] in
                guard let fetchedImage = operation?.fetchedImage else { return }
                self.cache.setObject(fetchedImage, forKey: character)
                
                self.serialAccessQueue.addOperation {
                    self.invokeCompletionHandler(for: character, with: fetchedImage)
                }
            }
            
            fetchQueue.addOperation(operation)
        }
    }

    private func operation(for character: Character) -> AsyncFetcherOperation? {
        for case let fetchOperation as AsyncFetcherOperation in fetchQueue.operations
        where !fetchOperation.isCancelled && fetchOperation.character.id == character.id {
            return fetchOperation
        }
        
        return nil
    }

    private func invokeCompletionHandler(for character: Character, with fetchedImage: UIImage) {
        let completionHandler = self.completionHandlers[character]
        self.completionHandlers[character] = nil
        completionHandler?(fetchedImage)
    }
}

import Foundation
import UIKit

class AsyncFetcherOperation: Operation {
    // MARK: Properties

    let character: Character

    private(set) var fetchedImage: UIImage?

    // MARK: Initialization

    init(character: Character) {
        self.character = character
    }

    // MARK: Operation overrides

    override func main() {
        guard let imageURL = URL(string: character.imageUrlPath),
              let data = try? Data(contentsOf: imageURL) else {
            return
        }
        let image = UIImage(data: data)
        guard !isCancelled else { return }
        fetchedImage = image
    }
}

import Foundation
import UIKit

enum CharacterListViewEvent {
    case viewDidLoad(collectionView: UICollectionView)
    case didTapFilterButton
    case didTapItemAt(indexPath: IndexPath)
    case searchTextChanged(String)
    case didTapRefresh
}

protocol CharacterListViewModelling {
    var delegate: CharacterListViewModelDelegate? { get set }
    func handle(_ event: CharacterListViewEvent)
}

protocol CharacterListViewModelDelegate {
    func showError(error: BBError)
}

class CharacterListViewModel: CharacterListViewModelling, FilterDelegate {

    // MARK: - private properties

    private enum Section {
      case main
    }
    private typealias DataSource = UICollectionViewDiffableDataSource<Section, Character>
    private typealias SnapShot = NSDiffableDataSourceSnapshot<Section, Character>
    private let imageFetcher: ImageFetcher
    private var dataSource: DataSource?
    private let api: BreakingBadAPIType
    private let coordinator: ApplicationCoordinatorType
    private var allCharacters: [Character] = [] {
        didSet {
            filterCharacters()
        }
    }
    private var searchText: String = "" {
        didSet {
            filterCharacters()
        }
    }
    private var filteredCharacters: [Character] = []

    // MARK: - init

    init(coordinator: ApplicationCoordinatorType,
         api: BreakingBadAPIType,
         imageFetcher: ImageFetcher = AsyncFetcher()) {
        self.coordinator = coordinator
        self.api = api
        self.imageFetcher = imageFetcher
    }

    // MARK: - interface
    var delegate: CharacterListViewModelDelegate?
    var selectedSeasons: [Int] = [] {
        didSet {
            filterCharacters()
        }
    }

    func handle(_ event: CharacterListViewEvent) {
        switch event {
        case .viewDidLoad(let collectionView):
            dataSource = makeDataSource(collectionView: collectionView)
            loadData()

        case .didTapFilterButton:
            coordinator.launchSeasonFilter(filterDelegate: self)

        case .didTapItemAt(let indexPath):
            guard let character = dataSource?.itemIdentifier(for: indexPath) else {
              return
            }
            coordinator.launchCharacterDetail(for: character,
                                              imageFetcher: imageFetcher)

        case .searchTextChanged(let text):
            searchText = text

        case .didTapRefresh:
            loadData()

        }
    }

    // MARK: - Private Helper methods

    private func applySnapshot(characters: [Character],
                       animatingDifferences: Bool = true) {
      var snapshot = SnapShot()
      snapshot.appendSections([.main])
      snapshot.appendItems(characters)
      dataSource?.apply(snapshot, animatingDifferences: animatingDifferences)
    }

    private func makeDataSource(collectionView: UICollectionView) -> DataSource {
      let dataSource = DataSource(collectionView: collectionView) { (collectionView, indexPath, character) -> UICollectionViewCell? in
        let cell: CharacterListCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        let characterCellViewModel = CharacterCellViewModel(name: character.name,
                                                            imageState: self.imageFetcher.fetchedImage(for: character))
        cell.configure(characterCellViewModel)
        if case .loading = characterCellViewModel.imageState {
            self.fetchImage(for: character)
        }
        return cell
      }
      return dataSource
    }

    private func filterCharacters() {
        filteredCharacters = allCharacters
            .filter(Character.nameFilter(for: self.searchText))
            .filter(Character.seasonFilter(for: self.selectedSeasons))
        applySnapshot(characters: filteredCharacters)
    }

    private func loadData() {
        self.api.getCharacters { [weak self] result  in
            switch result {
            case .failure(let error):
                self?.allCharacters = []
                self?.delegate?.showError(error: error)
            case .success(let characters):
                self?.allCharacters = characters
            }
        }
    }

    private func fetchImage(for character: Character) {
        imageFetcher.fetchImageAsync(for: character) { image in
            DispatchQueue.main.async { [weak self] in
                var snapshot = self?.dataSource?.snapshot()
                snapshot?.reloadItems([character])
                self?.dataSource?.apply(snapshot!)
            }
        }
    }
}

private extension Character {
    typealias Filter<T> = (T) -> Bool

    static func nameFilter(for searchStr: String) -> Filter<Character>  {
        guard searchStr != "" else {
            return { _ in
                true
            }
        }
        return {
            $0.name.lowercased().contains(searchStr.lowercased())
        }
    }

    static func seasonFilter(for selectedSeasons: [Int]) -> Filter<Character> {
        return {
            selectedSeasons.allSatisfy($0.appearance.contains)
        }
    }
}

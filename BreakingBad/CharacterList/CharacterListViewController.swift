import UIKit

class CharacterListViewController: UIViewController {

    // MARK: - Private properties
    private var viewModel: CharacterListViewModelling

    // MARK: - Initializers
    init(viewModel: CharacterListViewModelling) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = 1.0
        flowLayout.minimumLineSpacing = 1.0
        flowLayout.sectionInset = .zero
        return flowLayout
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView =  UICollectionView(frame: .zero,
                                               collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.setCollectionViewLayout(flowLayout, animated: false)
        collectionView.backgroundColor = .clear
        collectionView.contentInset = .zero
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let cellReuseIdentifier = CharacterListCollectionViewCell.reuseIdentifier
        collectionView.register(CharacterListCollectionViewCell.self,
                                forCellWithReuseIdentifier: cellReuseIdentifier)
        return collectionView
    }()

    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = "Enter a character name"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        return searchController
    }()

    private lazy var filterBarButtonItem: UIBarButtonItem = {
        let image = UIImage(systemName: "line.horizontal.3.decrease")?.withRenderingMode(.alwaysTemplate)
        return UIBarButtonItem(image: image,
                               style: .plain,
                               target: self, action: #selector(didTapFilter(_:)))
    }()

    private lazy var refreshBarButtonItem: UIBarButtonItem = {
        let image = UIImage(systemName: "arrow.clockwise")?.withRenderingMode(.alwaysTemplate)
        return UIBarButtonItem(image: image,
                               style: .plain,
                               target: self, action: #selector(didTapRefresh(_:)))
    }()

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureCollectionView()
        viewModel.handle(.viewDidLoad(collectionView: collectionView))
    }

    // MARK: - View Config
    private func configureNavigationBar() {
        self.navigationItem.title = "Breaking Bad Characters"
        self.navigationItem.searchController = searchController
        self.navigationItem.leftBarButtonItem = refreshBarButtonItem
        self.navigationItem.rightBarButtonItem = filterBarButtonItem
    }

    private func configureCollectionView() {
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor)
        ])
    }

    // MARK: - Actions
    @objc private func didTapFilter(_ sender: UIBarButtonItem) {
        viewModel.handle(.didTapFilterButton)
    }

    @objc private func didTapRefresh(_ sender: UIBarButtonItem) {
        viewModel.handle(.didTapRefresh)
    }
}

// MARK: - UISearchResultsUpdating
extension CharacterListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.handle(.searchTextChanged(searchController.searchBar.text ?? ""))
    }
}

// MARK: - UICollectionViewDelegate
extension CharacterListViewController: UICollectionViewDelegate,
                                       UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        viewModel.handle(.didTapItemAt(indexPath: indexPath))
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCellsPerRow: CGFloat = 3.0
        let cellSizeRatio: CGFloat = 2.0/3.0
        let cellWidth = (collectionView.frame.size.width - 2.0) / numberOfCellsPerRow
        let cellHeight = cellWidth / cellSizeRatio

        return CGSize(width: floor(cellWidth),
                      height: cellHeight)
    }
}

// MARK: CharaterListViewModelDelegate
extension CharacterListViewController: CharacterListViewModelDelegate {
    func showError(error: BBError) {
        self.showError("Error", message: error.localizedDescription)
    }

    func refresh() {
        collectionView.reloadData()
    }

    func refresh(at index: Int) {
        collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
    }
}

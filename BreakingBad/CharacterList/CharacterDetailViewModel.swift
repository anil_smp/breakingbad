import Foundation
import UIKit

enum CharacterDetailViewEvent {
    case viewDidLoad
}

protocol CharacterDetailViewModelling {
    var name: String { get }
    var occupation: String { get }
    var status: String { get }
    var nickname: String { get }
    var seasonsAppeared: String { get }
    var delegate: CharaterDetailViewModelDelegate? { get set }
    func handle(_ event: CharacterDetailViewEvent)
}

protocol CharaterDetailViewModelDelegate {
    func imageFetched(_ image: UIImage?)
}

class CharacterDetailViewModel: CharacterDetailViewModelling {

    var delegate: CharaterDetailViewModelDelegate?

    private let character: Character
    private let imageFetcher: ImageFetcher

    var name: String {
        character.name
    }
    var occupation: String {
        character.occupation
                 .joined(separator: ", ")
    }
    var status: String {
        character.status
    }
    var nickname: String {
        character.nickname
    }
    var seasonsAppeared: String {
        guard !character.appearance.isEmpty else {
            return "-"
        }
        return character.appearance
                        .compactMap(String.init)
                        .joined(separator: ", ")
    }

    init(character: Character,
         imageFetcher: ImageFetcher) {
        self.character = character
        self.imageFetcher = imageFetcher
    }

    func handle(_ event: CharacterDetailViewEvent) {
        switch event {
        case .viewDidLoad:
            imageFetcher.fetchImageAsync(for: character) { image in
                DispatchQueue.main.async { [weak self] in
                    self?.delegate?.imageFetched(image)
                }
            }
        }
    }
}


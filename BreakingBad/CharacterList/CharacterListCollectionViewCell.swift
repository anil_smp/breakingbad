import UIKit

class CharacterListCollectionViewCell: UICollectionViewCell {

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
        configureImageView()
        configureLabel()
        configureActivityIndiactor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Views
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private lazy var activityndicator: UIActivityIndicatorView = {
        let activityndicator = UIActivityIndicatorView(style: .medium)
        activityndicator.color = .orange
        activityndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityndicator
    }()

    private lazy var blurView: UIVisualEffectView = {
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        blurView.translatesAutoresizingMaskIntoConstraints = false
        return blurView
    }()

    private lazy var label: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: - View Setup
    private func configureImageView() {
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.rightAnchor.constraint(equalTo: rightAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    private func configureLabel() {
        addSubview(blurView)
        addSubview(label)
        NSLayoutConstraint.activate([
            label.heightAnchor.constraint(greaterThanOrEqualToConstant: 40.0),
            label.leftAnchor.constraint(equalTo: leftAnchor, constant: 5.0),
            label.rightAnchor.constraint(equalTo: rightAnchor, constant: -5.0),
            label.bottomAnchor.constraint(equalTo: bottomAnchor),
            blurView.topAnchor.constraint(equalTo: label.topAnchor),
            blurView.leftAnchor.constraint(equalTo: leftAnchor),
            blurView.rightAnchor.constraint(equalTo: rightAnchor),
            blurView.bottomAnchor.constraint(equalTo: label.bottomAnchor)
        ])
    }

    func configureActivityIndiactor() {
        addSubview(activityndicator)
        NSLayoutConstraint.activate([
            activityndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }

    // MARK: - Public interface
    func configure(_ cellViewModel: CharacterCellViewModel) {
        label.text = cellViewModel.name
        switch cellViewModel.imageState {
        case .loading:
            imageView.image = nil
            activityndicator.startAnimating()
            activityndicator.isHidden = false
        case .loaded(let image):
            imageView.image = image
            activityndicator.stopAnimating()
            activityndicator.isHidden = true
        }
    }
}

import Foundation
import UIKit

enum ImageState: Equatable {
    case loading
    case loaded(UIImage)
}

class CharacterCellViewModel {

    let name: String
    let imageState: ImageState

    init(name: String,
         imageState: ImageState){
        self.name = name
        self.imageState = imageState
    }
}

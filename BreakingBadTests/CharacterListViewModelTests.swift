import XCTest
@testable import BreakingBad

class CharacterListViewModelTests: XCTestCase {

    func testApiRequestMadeOnViewLoad() throws {
        let mockAPI = BBAPIMock()
        mockAPI.getCharactersResult = .success([Character.mock])
        let mockAppCoordinator = ApplicationCoordinatorMock()
        let characterListViewModel: CharacterListViewModelling =
            CharacterListViewModel(coordinator: mockAppCoordinator,
                                   api: mockAPI)
        characterListViewModel.handle(.viewDidLoad(collectionView: getCollectionView()))
        XCTAssertTrue(mockAPI.getCharactersCalled)
    }

    func testErrorShownOnApiCallFailing() throws {
        let mockAPI = BBAPIMock()
        mockAPI.getCharactersResult = .failure(BBError.unknownError)
        let mockAppCoordinator = ApplicationCoordinatorMock()
        var characterListViewModel: CharacterListViewModelling =
            CharacterListViewModel(coordinator: mockAppCoordinator,
                                   api: mockAPI)
        let mockDelegate = CharacterListViewModelDelegateMock()
        characterListViewModel.delegate = mockDelegate
        characterListViewModel.handle(.viewDidLoad(collectionView: getCollectionView()))
        XCTAssertTrue(mockDelegate.showErrorCalled)
        XCTAssertEqual(mockDelegate.showErrorParams!, .unknownError)
    }

    func testSeasonFilterLaunchedOnFilterButtonTap() throws {
        let mockAPI = BBAPIMock()
        mockAPI.getCharactersResult = .failure(BBError.unknownError)
        let mockAppCoordinator = ApplicationCoordinatorMock()
        let characterListViewModel: CharacterListViewModelling =
            CharacterListViewModel(coordinator: mockAppCoordinator,
                                   api: mockAPI)
        characterListViewModel.handle(.didTapFilterButton)
        XCTAssertTrue(mockAppCoordinator.launchSeasonFilterCalled)
    }

    func testEnteringSearchTextFiltersCharacters() throws {
        let mockAPI = BBAPIMock()
        mockAPI.getCharactersResult = .success(Character.mocks)
        let mockAppCoordinator = ApplicationCoordinatorMock()
        let characterListViewModel: CharacterListViewModelling =
            CharacterListViewModel(coordinator: mockAppCoordinator,
                                   api: mockAPI)
        let collectionView = getCollectionView()
        characterListViewModel.handle(.viewDidLoad(collectionView: collectionView))
        characterListViewModel.handle(.searchTextChanged("Walt"))
        XCTAssertTrue(collectionView.visibleCells.count == 1)
        characterListViewModel.handle(.searchTextChanged("n"))
        XCTAssertTrue(collectionView.visibleCells.count == 2)
    }

    func testChoosingSeasonsFiltersCharacters() throws {
        let mockAPI = BBAPIMock()
        mockAPI.getCharactersResult = .success(Character.mocks)
        let mockAppCoordinator = ApplicationCoordinatorMock()
        var characterListViewModel: CharacterListViewModelling & FilterDelegate =
            CharacterListViewModel(coordinator: mockAppCoordinator,
                                   api: mockAPI)
        let collectionView = getCollectionView()
        characterListViewModel.handle(.viewDidLoad(collectionView: collectionView))
        characterListViewModel.selectedSeasons = [1]
        XCTAssertTrue(collectionView.visibleCells.count == 1)
        characterListViewModel.selectedSeasons = [2]
        XCTAssertTrue(collectionView.visibleCells.count == 2)
        characterListViewModel.selectedSeasons = [3]
        XCTAssertTrue(collectionView.visibleCells.count == 1)
    }

    private func getCollectionView() -> UICollectionView {
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: UICollectionViewFlowLayout())
        let cellReuseIdentifier = CharacterListCollectionViewCell.reuseIdentifier
        collectionView.register(CharacterListCollectionViewCell.self,
                                forCellWithReuseIdentifier: cellReuseIdentifier)
        return collectionView
    }
}

private extension Character {
    static let mock = Character(id: 1,
                                name: "Bond",
                                occupation: ["Secret Agent", "Spy"],
                                imageUrlPath: "http://google.com",
                                status: "Alive",
                                optionalAppearance: [1, 2],
                                nickname: "Bond, James Bond")
    static let mocks = [
        Character(id: 1,
                  name: "Bond",
                  occupation: ["Secret Agent", "Spy"],
                  imageUrlPath: "http://google.com",
                  status: "Alive",
                  optionalAppearance: [1, 2],
                  nickname: "Bond, James Bond"),
        Character(id: 2,
                  name: "Waltern",
                  occupation: ["Drug kingpin", "Drug dealer"],
                  imageUrlPath: "http://google.com",
                  status: "Alive",
                  optionalAppearance: [2, 3],
                  nickname: "Heisenberg")
    ]
}

import Foundation
@testable import BreakingBad

class ApplicationCoordinatorMock: ApplicationCoordinatorType {

    var launchSeasonFilterCalled: Bool = false
    var launchCharacterDetailCalled: Bool = false
    var dismissSeasonFilterCalled: Bool = false

    init() { }

    func launchSeasonFilter(filterDelegate: FilterDelegate) {
        launchSeasonFilterCalled = true
    }

    func launchCharacterDetail(for character: Character,
                               imageFetcher: ImageFetcher) {
        launchCharacterDetailCalled = true
    }

    func dismissSeasonFilter() {
        dismissSeasonFilterCalled = true
    }
}

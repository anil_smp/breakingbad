import Foundation
@testable import BreakingBad

class CharacterListViewModelDelegateMock: CharacterListViewModelDelegate {

    var showErrorCalled: Bool = false
    var showErrorParams: BBError?

    init() { }

    func showError(error: BBError) {
        showErrorCalled = true
        showErrorParams = error
    }
}

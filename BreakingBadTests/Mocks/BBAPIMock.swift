import Foundation
@testable import BreakingBad

class BBAPIMock: BreakingBadAPIType {

    var getCharactersResult: Result<[Character], BBError>? = nil
    var getCharactersCalled: Bool = false

    init() { }
    
    func getCharacters(completion: @escaping ResultHandler<[Character]>) {
        getCharactersCalled = true
        completion(getCharactersResult!)
    }
}
